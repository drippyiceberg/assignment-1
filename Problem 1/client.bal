import ballerina/io;
import ballerina/http;

final http:Client clientEndpoint = check new ("http://localhost:6000");
public function main() returns error? {
    io:println("Hello World!");
    json studentprof = {
                username: "219088004",
                lastname: "Steven",
                firstname: "Naboth",
                preferred_formats: ["audio", "video", "text"],
                past_subjects: [
                        {
                        "course": "DSA",
                        score: "B+"
                        },
                        {
                        course: "Programming I",
                        score: "A+"
                        }
                    ],
                learning_materials:
                };

   // learning materials 
     json learning_materials =   {
            course: "Distributed Systems Applications",
            learning_objects: {
            required: {
            audio: [
                  {
                    name: "Topic 1",
                    description: "",
                    difficulty: ""
                 }
            ],
            text: [
                {

               }
              ]
            },
            suggested: {
                    video: [],
                    audio: []
               }
            }
     };

    //string value = check clientEndpoint->get("/hello/Update/'dd'/22");


    json updatedstudentprof = {
                username: "errr",
                preferred_formats: ["audio", "video","text"],
                past_subjects: [
                        {
                        "course": "Algorithms",
                        score: "B+"
                        },
                        {
                        "course": "ERP",
                        score: "B+"
                        },
                        {
                        course: "Programming I",
                        score: "A+"
                        },
                        {
                        course: "Mathematics",
                        score: "C+"
                        },
                        {
                        course: "Database",
                        score: "B+"
                        }
                    ]
                };
   

    io:println("************* DSA REPO **********");
    io:println("1. Create Function");
    io:println("2. Update Function");
    io:println("3. Create Learning Material ");
    io:println("----------------------------------------");
    string choose = io:readln("Enter Option: ");
    if (choose === "1"){
        json value = check clientEndpoint->post("/createLearnerProfile",studentprof);
        io:println(value);
    }else if (choose === "2") {
       json updatedvalue = check clientEndpoint->post("/updateLeanerProfile",updatedstudentprof);
       io:println(updatedvalue);
     }else if (choose === "3") {
         json value = check clientEndpoint->post("/createLearningMaterial",learning_materials);
     }
}
