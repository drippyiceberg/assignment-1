import ballerina/io;
import ballerina/http;
import ballerinax/mongodb;
//connecting to the mongo database 
mongodb:ClientConfig mongoConfig = {
    host: "localhost",
    port: 27017,
    // username: <DB_USERNAME>,
     //password: <DB_PASSWORD>,
    options: {sslEnabled: false, serverSelectionTimeout: 5000}
};
//conndect to the database
mongodb:Client mongoClient = check new (mongoConfig, "DSA_Assignment_1");

service / on new http:Listener(6000) {
// for creating a learner
    resource function post createLearnerProfile(@http:Payload json learnerprofile) returns json|error? {
        io:println(learnerprofile.toJsonString());
        map<json> studentprofile = <map<json>>learnerprofile;
         //inserting learner information in database
        checkpanic mongoClient->insert(studentprofile,"learner_profile");
        // mongoClient->close();
        return learnerprofile;
    }
//updating a learner profile using post
    resource function post updateLeanerProfile(@http:Payload json newLprofile) returns json|error?{

        string message="";
        map<json> updatedprof = <map<json>>newLprofile;
        map<json>|error updatefilter = {"username":check updatedprof.username};
        if(updatefilter is error){
            io:println("Error");
        }else{
            int response = checkpanic mongoClient->update(updatedprof, "learner_profile", (), updatefilter , true);
            if (response > 0 ) {
                io:println("Modified count: '" + response.toString() + "'.") ;
                message = "succesfully Updated!";
            } else {
                io:println("Nothing Updated.");
                message = "nothing Updated!";
            }
        }
        // map<json> updatefilter = {"username":"makosaisaac@makosa"};
        // first value is the updated data, second is collection name, (), forth value is the filter or the key , true

        json respo = {message:message};
        return respo;
    }

     //resource function get Update/[string name]/[int age]()returns string {
     //    io:println(name,age);
     //    return "Hello world";
    // }
    // for creating a learner
    resource function post createLearningMaterial(@http:Payload json learnerprofile) returns json|error? {
        io:println(learnerprofile.toJsonString());
        map<json> learningMaterial = <map<json>>learnerprofile;
         //inserting learner information in database
        checkpanic mongoClient->insert(learningMaterial,"learning_material");
        // mongoClient->close();
        return learnerprofile;
    }
    resource function post getLearnersMaterials(@http:Payload json learnerprofile) returns json {
        json allLearnerstopics = {};

        //  map<json> queryString = {"course":  learnerprofile.course };
        //  map<json>[] allfns = checkpanic mongoClient->find("learning_material", (), queryString);
        return allLearnerstopics;
    }
}