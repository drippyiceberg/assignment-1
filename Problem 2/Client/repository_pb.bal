import ballerina/grpc;

public isolated client class DSARepositoryClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function add_new_fn(FUNCTION|ContextFUNCTION req) returns (response_message|grpc:Error) {
        map<string|string[]> headers = {};
        FUNCTION message;
        if (req is ContextFUNCTION) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("DSARepository/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <response_message>result;
    }

    isolated remote function add_new_fnContext(FUNCTION|ContextFUNCTION req) returns (ContextResponse_message|grpc:Error) {
        map<string|string[]> headers = {};
        FUNCTION message;
        if (req is ContextFUNCTION) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("DSARepository/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <response_message>result, headers: respHeaders};
    }

    isolated remote function delete_fn(string|ContextString req) returns (response_message|grpc:Error) {
        map<string|string[]> headers = {};
        string message;
        if (req is ContextString) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("DSARepository/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <response_message>result;
    }

    isolated remote function delete_fnContext(string|ContextString req) returns (ContextResponse_message|grpc:Error) {
        map<string|string[]> headers = {};
        string message;
        if (req is ContextString) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("DSARepository/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <response_message>result, headers: respHeaders};
    }

    isolated remote function show_fn(show_all_fn_ID|ContextShow_all_fn_ID req) returns (FUNCTION|grpc:Error) {
        map<string|string[]> headers = {};
        show_all_fn_ID message;
        if (req is ContextShow_all_fn_ID) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("DSARepository/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <FUNCTION>result;
    }

    isolated remote function show_fnContext(show_all_fn_ID|ContextShow_all_fn_ID req) returns (ContextFUNCTION|grpc:Error) {
        map<string|string[]> headers = {};
        show_all_fn_ID message;
        if (req is ContextShow_all_fn_ID) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("DSARepository/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <FUNCTION>result, headers: respHeaders};
    }

    isolated remote function add_fns() returns (Add_fnsStreamingClient|grpc:Error) {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("DSARepository/add_fns");
        return new Add_fnsStreamingClient(sClient);
    }

    isolated remote function show_all_fns(string|ContextString req) returns stream<FUNCTION, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if (req is ContextString) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("DSARepository/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        FUNCTIONStream outputStream = new FUNCTIONStream(result);
        return new stream<FUNCTION, grpc:Error?>(outputStream);
    }

    isolated remote function show_all_fnsContext(string|ContextString req) returns ContextFUNCTIONStream|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if (req is ContextString) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("DSARepository/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        FUNCTIONStream outputStream = new FUNCTIONStream(result);
        return {content: new stream<FUNCTION, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function show_all_with_criteria() returns (Show_all_with_criteriaStreamingClient|grpc:Error) {
        grpc:StreamingClient sClient = check self.grpcClient->executeBidirectionalStreaming("DSARepository/show_all_with_criteria");
        return new Show_all_with_criteriaStreamingClient(sClient);
    }
}

public client class Add_fnsStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendFUNCTION(FUNCTION message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextFUNCTION(ContextFUNCTION message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveResponse_message() returns response_message|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return <response_message>payload;
        }
    }

    isolated remote function receiveContextResponse_message() returns ContextResponse_message|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <response_message>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public class FUNCTIONStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|FUNCTION value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|FUNCTION value;|} nextRecord = {value: <FUNCTION>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public client class Show_all_with_criteriaStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendString(string message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextString(ContextString message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveFUNCTION() returns FUNCTION|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return <FUNCTION>payload;
        }
    }

    isolated remote function receiveContextFUNCTION() returns ContextFUNCTION|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <FUNCTION>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class DSARepositoryResponsemessageCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendResponse_message(response_message response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextResponse_message(ContextResponse_message response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class DSARepositoryFUNCTIONCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendFUNCTION(FUNCTION response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextFUNCTION(ContextFUNCTION response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextStringStream record {|
    stream<string, error?> content;
    map<string|string[]> headers;
|};

public type ContextFUNCTIONStream record {|
    stream<FUNCTION, error?> content;
    map<string|string[]> headers;
|};

public type ContextResponse_message record {|
    response_message content;
    map<string|string[]> headers;
|};

public type ContextString record {|
    string content;
    map<string|string[]> headers;
|};

public type ContextShow_all_fn_ID record {|
    show_all_fn_ID content;
    map<string|string[]> headers;
|};

public type ContextFUNCTION record {|
    FUNCTION content;
    map<string|string[]> headers;
|};

public type response_message record {|
    string message = "";
|};

public type fn_metadata record {|
    string language = "";
    Developer developer = {};
    string[] keywords = [];
|};

public type Developer record {|
    string fullname = "";
    string email = "";
|};

public type show_all_fn_ID record {|
    int 'version = 0;
    string fn_id = "";
|};

public type FUNCTION record {|
    string fn_data = "";
    string fn_id = "";
    fn_metadata fn_metadata = {};
    RepoType repo_type = PRIVATE;
    int fn_version = 0;
|};

public enum RepoType {
    PRIVATE,
    PUBLIC
}

const string ROOT_DESCRIPTOR = "0A107265706F7369746F72792E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F223D0A09446576656C6F706572121A0A0866756C6C6E616D65180120012809520866756C6C6E616D6512140A05656D61696C1802200128095205656D61696C226F0A0B666E5F6D65746164617461121A0A086C616E677561676518012001280952086C616E677561676512280A09646576656C6F70657218022001280B320A2E446576656C6F7065725209646576656C6F706572121A0A086B6579776F72647318032003280952086B6579776F72647322AE010A0846554E4354494F4E12170A07666E5F646174611801200128095206666E4461746112130A05666E5F69641802200128095204666E4964122D0A0B666E5F6D6574616461746118032001280B320C2E666E5F6D65746164617461520A666E4D6574616461746112260A097265706F5F7479706518042001280E32092E5265706F5479706552087265706F54797065121D0A0A666E5F76657273696F6E1805200128055209666E56657273696F6E222C0A10726573706F6E73655F6D65737361676512180A076D65737361676518012001280952076D657373616765223F0A0E73686F775F616C6C5F666E5F494412180A0776657273696F6E180120012805520776657273696F6E12130A05666E5F69641802200128095204666E49642A230A085265706F54797065120B0A07505249564154451000120A0A065055424C4943100132CD020A0D4453415265706F7369746F7279122A0A0A6164645F6E65775F666E12092E46554E4354494F4E1A112E726573706F6E73655F6D65737361676512290A076164645F666E7312092E46554E4354494F4E1A112E726573706F6E73655F6D6573736167652801123C0A0964656C6574655F666E121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A112E726573706F6E73655F6D65737361676512250A0773686F775F666E120F2E73686F775F616C6C5F666E5F49441A092E46554E4354494F4E12390A0C73686F775F616C6C5F666E73121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A092E46554E4354494F4E300112450A1673686F775F616C6C5F776974685F6372697465726961121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A092E46554E4354494F4E28013001620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"google/protobuf/wrappers.proto": "0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "repository.proto": "0A107265706F7369746F72792E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F223D0A09446576656C6F706572121A0A0866756C6C6E616D65180120012809520866756C6C6E616D6512140A05656D61696C1802200128095205656D61696C226F0A0B666E5F6D65746164617461121A0A086C616E677561676518012001280952086C616E677561676512280A09646576656C6F70657218022001280B320A2E446576656C6F7065725209646576656C6F706572121A0A086B6579776F72647318032003280952086B6579776F72647322AE010A0846554E4354494F4E12170A07666E5F646174611801200128095206666E4461746112130A05666E5F69641802200128095204666E4964122D0A0B666E5F6D6574616461746118032001280B320C2E666E5F6D65746164617461520A666E4D6574616461746112260A097265706F5F7479706518042001280E32092E5265706F5479706552087265706F54797065121D0A0A666E5F76657273696F6E1805200128055209666E56657273696F6E222C0A10726573706F6E73655F6D65737361676512180A076D65737361676518012001280952076D657373616765223F0A0E73686F775F616C6C5F666E5F494412180A0776657273696F6E180120012805520776657273696F6E12130A05666E5F69641802200128095204666E49642A230A085265706F54797065120B0A07505249564154451000120A0A065055424C4943100132CD020A0D4453415265706F7369746F7279122A0A0A6164645F6E65775F666E12092E46554E4354494F4E1A112E726573706F6E73655F6D65737361676512290A076164645F666E7312092E46554E4354494F4E1A112E726573706F6E73655F6D6573736167652801123C0A0964656C6574655F666E121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A112E726573706F6E73655F6D65737361676512250A0773686F775F666E120F2E73686F775F616C6C5F666E5F49441A092E46554E4354494F4E12390A0C73686F775F616C6C5F666E73121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A092E46554E4354494F4E300112450A1673686F775F616C6C5F776974685F6372697465726961121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A092E46554E4354494F4E28013001620670726F746F33"};
}

