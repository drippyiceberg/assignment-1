import ballerina/io;
import ballerina/grpc;
DSARepositoryClient ep = check new ("http://localhost:8000");

public function main() {
    io:println("************* DSA REPO **********");
    io:println("1. ADD New Function");
    io:println("2. ADD Functions");
    io:println("3. Delete Function");
    io:println("4. Show Function");
    io:println("5. Show All Functions");
    io:println("6. Show all with Criteria Function");
    io:println("----------------------------------------");
    string choose = io:readln("Enter Option: ");

    if (choose === "1"){
        add_new_fn();
    }else if (choose === "2") {
        error? fns = add_fns();
        if fns is error {
            io:println(fns);
        }
    }else if (choose === "3") {
        delete_fn();
    }else if (choose === "4") {
        show_fn();
    }else if (choose === "5") {
        error? showAllfns = show_all_fns();
        if showAllfns is error {
            io:println("Error Occured");
        }
    }else if (choose === "6") {
        error? criteria = show_all_with_criteria();
        if (criteria is error){
            io:println("Error Criteria ...");
        }
    }

}

public function add_new_fn() {
    io:println("--------ADD New Functions----------");
    FUNCTION fncts = {
        fn_data: "ballerina project",
        fn_id:"function2",
        fn_metadata: {
            developer: {
                email: "florence@gmail.com",
                fullname: "Florence "
                },
                keywords: ["API","Protocol","Data"],
                language: "Java"
            },
        repo_type: PUBLIC,
        fn_version: 0
    };
    //adding a brand new function
    var respond = ep->add_new_fn(fncts);
    if respond is grpc:Error {
        io:println(respond.toString());
    } else {
        io:println(respond);
    }
    io:println("--------------------------------------");
    string back = io:readln("Click 1 to go back: ");
    var hello = (back === "1")?main():add_new_fn();
}

public function add_fns() returns error?{
    io:println("--------ADD Multiple Functions----------");
    //putting functions in an array
    FUNCTION [] gt = [];

    FUNCTION fncts = {
        fn_data: "fun1",
        fn_metadata: {
            developer: {
                email: "makosaisaac",
                fullname: "Isaac Makosa"
                },keywords: ["hagshhghsahf","ajsghgshasg","jahskjahskjshk"],
                language: "Ballerina"
            }
    };

    FUNCTION fncts1 = {
        fn_data: "fun2",
        fn_id:"functionwrreyyvdvbvcv",
        fn_metadata: {
            developer: {
                email: "makosaisaac",
                fullname: "Isaac Makosa"
                },keywords: ["hagshhghsahf","ajsghgshasg","jahskjahskjshk"],
                language: "Ballerina"
            }
    };

    FUNCTION fncts2 = {
        fn_data: "fun3",
        fn_id:"function2",
        fn_metadata: {
            developer: {
                email: "makosaisaac",
                fullname: "Isaac Makosa"
                },keywords: ["hagshhghsahf","ajsghgshasg","jahskjahskjshk"],
                language: "Ballerina"
            }
    };

    FUNCTION fncts3 = {
        fn_data: "fun4",
        fn_id:"function3",
        fn_metadata: {
            developer: {
                email: "makosaisaac",
                fullname: "Isaac Makosa"
                },keywords: ["hagshhghsahf","ajsghgshasg","jahskjahskjshk"],
                language: "Ballerina"
            }
    };
// pushing to gt array 
    gt.push(fncts);
    gt.push(fncts1);
    gt.push(fncts2);
    gt.push(fncts3);    
    
    Add_fnsStreamingClient streamclient = check ep->add_fns();
    //for loop we  insert in the database
    foreach FUNCTION item in gt {
        check streamclient->sendFUNCTION(item);
    }

    check streamclient->complete();

    json? response = check streamclient->receiveResponse_message();
    io:println(response);
    
    io:println("--------------------------------------");
    string back = io:readln("Click 1 to go back: ");
    var hello = (back === "1")?main():add_new_fn();
}

public function delete_fn() {
    io:println("-----------Deleting fns----------------");
    //specify the function to delete
    var respond =  ep->delete_fn("function2");
    if respond is grpc:Error {
        io:println(respond.toString());
    } else {
        io:println(respond);
    }
    io:println("--------------------------------------");
    string back = io:readln("Click 1 to go back: ");

    var hello = (back === "1")?main():add_new_fn();
}

public function show_fn() {
    io:println("------Show Functions with version--------");
    show_all_fn_ID versn_id = {"version":0,fn_id:"function3"};
    var respond =  ep->show_fn(versn_id);
    if respond is grpc:Error {
        io:println(respond.toString());
    } else {
        io:println(respond);
    }
    io:println("--------------------------------------");
    string back = io:readln("Click 1 to go back: ");

    var hello = (back === "1")?main():add_new_fn();
}

public function show_all_fns() returns error? {
    io:println("-----Show All Fns Latest Versions------");
    var respond =  ep->show_all_fns("hsghdfagdfhagdfgahsfdhg");
    if respond is grpc:Error {
        io:println(respond.toString());
    } else {
        check respond.forEach(function(FUNCTION fn){
            io:print(io:println(fn));
        });
    }
    io:println("-------------------------------------------");
    string back = io:readln("Click 1 to go back: ");

    var hello = (back === "1")?main():add_new_fn();
}

public function show_all_with_criteria() returns error?{
    io:println("--------------Show All with Criteria-----------------");
    string [] criteria = ["Java","Javascript","Python"];
    Show_all_with_criteriaStreamingClient showCriteria = check ep->show_all_with_criteria();

    foreach string item in criteria {
        check showCriteria->sendString(item);
    }
    check showCriteria->complete();

    FUNCTION|grpc:Error? fns = check showCriteria->receiveFUNCTION();
    while !(fns is ()) {
        io:println(fns);
        fns = check showCriteria->receiveFUNCTION();
    }
    io:println("----------------------------------------------------");
    string back = io:readln("Click 1 to go back: ");

    var hello = (back === "1")?main():add_new_fn();
}

