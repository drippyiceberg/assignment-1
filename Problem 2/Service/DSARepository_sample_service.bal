import ballerina/io;
import ballerina/grpc;
import ballerinax/mongodb;

mongodb:ClientConfig mongoConfig = {
    host: "localhost",
    port: 27017,
    options: {sslEnabled: false, serverSelectionTimeout: 7000}
};

mongodb:Client mongoClient = check new (mongoConfig,"DSAProblem2");
string collection_name = "FunctionsRepo";

listener grpc:Listener ep = new (8000);

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "DSARepository" on ep {

    remote function add_new_fn(FUNCTION value) returns response_message|error {
        io:println(value);
        map<json> save_new = {};
        save_new = <map<json>> value.toJson();
        checkpanic mongoClient->insert(save_new,collection_name);

        response_message ht = {message: "function saved successfully."};
        return ht;
    }
    remote function delete_fn(string value) returns response_message|error {
        response_message ht = {};
        io:println(value);
        map<json> deleteFilter = { "fn_id": value };
        int deleteRet = checkpanic mongoClient->delete(collection_name, (), deleteFilter, true);
        if (deleteRet > 0 ) {
            ht= {message:"Function deleted successfully"};
        } else {
            ht= {message:"Function delete failed"};
        }
        //mongoClient->close();
        return ht;
    }
    remote function show_fn(show_all_fn_ID value) returns FUNCTION|error {
            io:println(value);
            FUNCTION response = {};
            map<json> fn_filter = <map<json>>value.toJson();
            map<json>[] showfns = checkpanic mongoClient->find(collection_name, (), fn_filter);        

            if(showfns.length()>0){
                map<json> dt = showfns[0];
                var dt1 = dt.remove("_id");
                FUNCTION|error fns = showfns[0].cloneWithType(FUNCTION);
                io:println(fns);
                if (fns is error) {
                    io:println("Error Occured");
                }else{
                    response = fns;
                }
            }

            return response;
    }
    remote function add_fns(stream<FUNCTION, grpc:Error?> clientStream) returns response_message|error {
        response_message ht = {};
        check clientStream.forEach(function(FUNCTION fn){
            io:println(fn);
            checkpanic mongoClient->insert(fn,collection_name);
        });
        //mongoClient->close();

        ht = { message: "Function successfully added."};
        return ht;
    }
    remote function show_all_fns(string value) returns stream<FUNCTION, error?>|error {
            io:println(value);
            FUNCTION [] fnsky = []; 
            map<json> queryString = {"fn_id": value };
            map<json>[] allfns = checkpanic mongoClient->find(collection_name, (), queryString);
            foreach var data in allfns {
                json dt = data.remove("_id");
                FUNCTION|error fns = data.cloneWithType(FUNCTION);
                    io:println(fns);
                    if (fns is error) {
                        io:println("Error Occured");
                    }else {
                        fnsky.push(fns);
                    }
            }
            return fnsky.toStream();
    }
    remote function show_all_with_criteria(stream<string, grpc:Error?> clientStream) returns stream<FUNCTION, error?>|error {
        FUNCTION [] functions = [];
        map<json>[] fnsCriteria = checkpanic mongoClient->find(collection_name, (), ({}));
        check clientStream.forEach(function(string names){
            io:println(names);
            foreach var item in fnsCriteria {
                if (item.fn_metadata.language===names){
                    io:println(item);
                    // functions.push(<FUNCTION>item);
                    json item1 = item.remove("_id");
                    json item2 = item;
                    FUNCTION|error fns = item2.cloneWithType(FUNCTION);
                    io:println(fns);
                    if (fns is error) {
                        io:println("Error Occured");
                    }else {
                        functions.push(fns);
                    }
                }
            }
        });
        // mongoClient->close();
        return functions.toStream();
    }
}

